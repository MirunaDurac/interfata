package controllers;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Vector;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import model.Animal;
import model.Appointment;
import model.Istoric;
import model.Medicalstuff;
import main.DateTime;
import main.AppintementList;
import util.DatabaseUtil;
import javafx.scene.input.MouseEvent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.control.TextField;

public class MainController implements Initializable {
	@FXML
	 private ListView<String> mainListView;
	@FXML
	 private ListView<String> listView;
	@FXML
	 private Button addAppointment;
	private int idMed=1;
	private int idApp;
	private int idAnimal;
	private Vector<AppintementList> app=new Vector<>();
	private Map<String,Integer> animalIsAndName=new HashMap< String,Integer>();
	@FXML
	void addAppoinmentClicked(ActionEvent e) {
		System.out.println("test");
	}
	
	@FXML ImageView img;
	@FXML Button back;
	@FXML ListView istoric;
	@FXML Pane newPanel;
	@FXML TextField data;
	@FXML TextField ora;
	@FXML TextField id;
	@FXML Button addButton;
	@FXML TextField nume;

	public void populateMainListView() throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		List<Animal> animalDBList = (List<Animal>)db.printAllAnimalsFromBD();
		ObservableList<Animal> animalList = FXCollections.observableArrayList(animalDBList);
		mainListView.setItems(getAnimalNameObservableList(animalList));
		mainListView.refresh();
		db.closeEntityManager();
	}
	public ObservableList<String> getAnimalNameObservableList(List<Animal> animals) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Animal a: animals) {
			names.add(a.getName());
			animalIsAndName.put( a.getName(),a.getIdAnimal());
		}
		return names;
	}
	
	//listarea personalului medical
	public void populateMainListViewMedicalStuff() throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		List<Medicalstuff> animalDBList = (List<Medicalstuff>)db.printAllMedicalStuffFromBD();
		ObservableList<Medicalstuff> animalList = FXCollections.observableArrayList(animalDBList);
		mainListView.setItems(getNameMedicalStuffObservableList(animalList));
		mainListView.refresh();
		db.closeEntityManager();
	}
	public ObservableList<String> getNameMedicalStuffObservableList(List<Medicalstuff> medical) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Medicalstuff a: medical) {
			names.add(a.getName());
			
		}
		return names;
	}
	//animalul de la o anumita programare
		public void populateMainListViewAnimal() throws Exception {
			DatabaseUtil db = new DatabaseUtil();
			db.setUp();
			db.startTransaction();
			List<Appointment> appointmentDBList = (List<Appointment>)db.printAllAppointmentFromBD();
			ObservableList<Appointment> appointmentList = FXCollections.observableArrayList(appointmentDBList);
			mainListView.setItems(getAnimal(appointmentList));
			mainListView.refresh();
			db.closeEntityManager();
		}
	public ObservableList<String> getAnimal(List<Appointment> app) throws IOException {
		DatabaseUtil db = new DatabaseUtil();
		ObservableList<String> names = FXCollections.observableArrayList();
		List<Animal> animal = (List<Animal>)db.printAllAnimalsFromBD();
		for (Appointment a: app) {
			if(idApp==a.getIdappointment()){
				for(Animal an: animal) {
					if(an.getIdAnimal()==a.getIdAnimal()){
						idAnimal=an.getIdAnimal();
						names.add(an.getName());
						names.add(an.getNumeStapan());
						names.add(an.getType());
						int aux=an.getAge();
						StringBuilder s=new StringBuilder();
						s.append(aux);
						names.add(s.toString());
						 InputStream is=new ByteArrayInputStream( an.getPicture());
			                OutputStream os=new FileOutputStream(new File("img.jpg"));
			                byte [] content= new byte[1024];
			                int size=0;
			                    while ((size=is.read(content))!=-1){

			                        os.write(content, 0, size);
			                    }

			                os.close();
			                is.close();

			                javafx.scene.image.Image image1=new Image("file:img.jpg", img.getFitWidth(), img.getFitHeight(), true, true);
			                img.setImage(image1);
			                img.setPreserveRatio(true);
						break;
					}
					
				}
				break;
			}
		}
		
		return names;
	}
	

	public void populatelistView() throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		List<Appointment> appointmentDBList = (List<Appointment>)db.printAllAppointmentFromBD();
		ObservableList<Appointment> appointmentList = FXCollections.observableArrayList(appointmentDBList);
		listView.setItems(getDateObservableList(appointmentList));
		listView.refresh();
		db.closeEntityManager();
	}
	
	public ObservableList<String> getDateObservableList(List<Appointment> appointment ) {
		ObservableList<String> dates = FXCollections.observableArrayList();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    Date date = new Date();
		Vector<String>sortare=new Vector<String>();
		for (Appointment a: appointment) {
			Date d=a.getDate();
			String stri=d.toString();
			if(a.getIdMedicalStuff()==idMed)
			{
		//	if(stri==dateFormat.format(date)) {
				Date di=a.getHour();
				sortare.add(di.toString());
			//}
			}
		}
		
		
		Collections.sort(sortare,(p1,p2)->p1.compareTo(p2));
		for(String s: sortare){
				dates.add(s);
		}
		for(Appointment a: appointment ) {
			AppintementList ap=new AppintementList();
			ap.id=a.getIdappointment();
			Date di=a.getHour();
			ap.nume=di.toString();
			app.add(ap);
		}
		for(AppintementList a: app)
		{
			System.out.println(a.nume);
		}
		return dates;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		try {
			//populateMainListView();
			populateMainListViewMedicalStuff();
			//populatelistView();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	@FXML public void first(MouseEvent event) throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		List<Medicalstuff> persMedDBList = (List<Medicalstuff>)db.printAllMedicalStuffFromBD();
		mainListView.setVisible(false);
		
			idMed=mainListView.getSelectionModel().getSelectedIndex()+1;
			System.out.println("Selected index: "+idMed);
			populatelistView();
			listView.setVisible(true);
	}
	@FXML public void appointement(MouseEvent event) throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		List<Appointment> persMedDBList = (List<Appointment>)db.printAllAppointmentFromBD();
			String aux=listView.getSelectionModel().getSelectedItem();
			for(AppintementList a: app){
				if(aux.equals(a.nume)) {
					idApp=a.id;
				}
			}
			System.out.println("Selected index: "+idApp);
			listView.setVisible(true);
			mainListView.setVisible(true);
			populateMainListViewAnimal();
			populateIstoric();
			
	}
	@FXML public void restart(ActionEvent event) throws Exception {
		listView.setVisible(false);
		mainListView.setVisible(true);
		newPanel.setVisible(false);
		addButton.setVisible(false);
		populateMainListViewMedicalStuff();
	}
	public void populateIstoric() throws Exception {
		DatabaseUtil db = new DatabaseUtil();
		db.setUp();
		db.startTransaction();
		List<Istoric> istoricDBList = (List<Istoric>)db.printAllIstoricFromBD();
		ObservableList<Istoric> istoricList = FXCollections.observableArrayList(istoricDBList);
		istoric.setItems(getIstoric(istoricList));
		istoric.refresh();
		db.closeEntityManager();
	}
	public ObservableList<String> getIstoric(List<Istoric> istoric) {
		ObservableList<String> names = FXCollections.observableArrayList();
		for (Istoric a: istoric) {
			if(a.getIdAnimal()==idAnimal)
			names.add(a.getType());
		}
		return names;
	}
	@FXML public void addAppoinment(ActionEvent event) throws Exception {
		populateMainListView();
		istoric.setVisible(false);
		newPanel.setVisible(true);
		addButton.setVisible(true);
		
	}
	@FXML public void okButton(ActionEvent event) throws Exception {
		
		String d=data.getText();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = format.parse(d);
		SimpleDateFormat forma = new SimpleDateFormat("HH:mm:ss");
		Time  hour = new Time(forma.parse(ora.getText()).getTime());
		int idAppo=Integer.parseInt(id.getText());
		Appointment ap=new Appointment();
		String numeAnimal=nume.getText();
		int idAnimal;
		idAnimal=animalIsAndName.get(numeAnimal);
		
		DatabaseUtil db = new DatabaseUtil();
		
		ap.setDate(date);
		ap.setHour(hour);
		ap.setIdAnimal(idAnimal);
		ap.setIdappointment(idAppo);
		ap.setIdMedicalStuff(idMed);
		
		db.setUp();
		db.startTransaction();
		db.saveAppointement(ap);
		db.comitTransaction();
		db.closeEntityManager();
		populatelistView();
		newPanel.setVisible(false);
		addButton.setVisible(false);
		istoric.setVisible(true);
	}

	
}
