package controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class Login implements Initializable {
	@FXML PasswordField password;
	@FXML TextField username;
	@FXML Button login;
	@FXML public void login(ActionEvent event) {
		try {
			Socket socket=new Socket("localhost", 5000);
			BufferedReader echoes=new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter stringEcho=new PrintWriter(socket.getOutputStream(),true);
			String echoString;
			String response;
				echoString=username.getText();
				stringEcho.println(echoString);
				echoString=password.getText();
				stringEcho.println(echoString);
				stringEcho.flush();
				response=echoes.readLine();
				if(response.equals("Login Failed"))
					{
						Alert alert = new Alert(AlertType.INFORMATION);
						alert.setTitle("INVALID");
						alert.setHeaderText("Username or password is wrong");
						alert.showAndWait().ifPresent(rs -> {
						    if (rs == ButtonType.OK) {
							        System.out.println("Pressed OK.");
							    }
							});
					}
				else {
					Parent root=FXMLLoader.load(getClass().getResource("/controllers/MainView.fxml"));
					Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
					Scene logScene=new Scene(root,700,700);
					
					Stage window =(Stage)((Node)event.getSource()).getScene().getWindow();
					window.setX(primaryScreenBounds.getMinX()+200  );
					window.setY(primaryScreenBounds.getMinY() );
					window.setScene(logScene);
					window.show();
				}
				 System.out.println("This is the response: " + response);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
	}
}
