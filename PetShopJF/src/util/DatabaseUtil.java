package util;

import java.sql.Time;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import model.Animal;
import model.Appointment;
import model.Istoric;
import model.Medicalstuff;
public class DatabaseUtil {
	public static EntityManagerFactory entityManagerFactory;
	public static EntityManager entityManager;
/**
 * make the connection
 * @throws Exception
 */
	public void setUp() throws Exception {
		entityManagerFactory = Persistence.createEntityManagerFactory("PetShopJF");
		entityManager = entityManagerFactory.createEntityManager();
	}
/**
 * save the animal in database
 * @param animal
 */
	public void saveAnimal(Animal animal) {
		entityManager.persist(animal);
	}
/**
 * to make the transaction of database
 */
	public void startTransaction() {
		entityManager.getTransaction().begin();
	}
/**
 * 
 */
	public void comitTransaction() {
		entityManager.getTransaction().commit();
	}
/**
 * to close the entity manager
 */
	public void closeEntityManager() {
		entityManager.close();
	}
/**
 * Print all animal form database
 */
	public List<Animal> printAllAnimalsFromBD() {
		List<Animal> results = entityManager.createNativeQuery("SELECT * FROM Animal ", Animal.class).getResultList();
//		for (Animal animal : results) {
//			System.out.println("Animal:" + animal.getName() + " has ID:" + animal.getIdAnimal()+"has age:"+animal.getAge()+"he is:"+ animal.getType());
//		}
		return results;
	}
/**
 * delete an animal 	
 * @param id the id that we are looking
 */
	public void deleteAnimal(int id) {
		Animal animal = entityManager.find(Animal.class, id);
		entityManager.remove(animal);
	}
	/**
	 * 
	 * @param a the animal that will be use to up date
	 * @param id of the animal that will be up date
	 */
	public void upDateAnimal(Animal a, int id) {
		Animal animal = entityManager.find(Animal.class, id);
		animal.setAge(a.getAge());
		//animal.setIdAnimal(a.getIdAnimal());
		animal.setName(a.getName());
		animal.setType(a.getType());
	}
	/**
	 * 
	 * @param a the animal 
	 * @param name of the animal
	 * @param id of the animal
	 * @param type of the animal
	 * @param age of the animal
	 */
	public void createAnimal(Animal a,String name, int id, String type, int age) {
		a.setIdAnimal(id);
		a.setName(name);
		a.setType(type);
		a.setAge(age);
	}

/**
 * the function that will save the medical stuff in database
 * @param med the object that will be save
 */
	public void saveMedicalStuff(Medicalstuff med) {
		entityManager.persist(med);
	}
	/**
	 * 
	 * @param m the medical stuff that will be create
	 * @param name of the medical stuff that will be create
	 * @param id of the medical stuff that will be create
	 * @param type of the medical stuff that will be create
	 */
	public void createMedicalStuff(Medicalstuff m, String name, int id, String type) {
		m.setIdMedicalStuff(id);
		m.setName(name);
		m.setType(type);
	}
	/**
	 * Print all medical stuff from database
	 */
	public List<Medicalstuff> printAllMedicalStuffFromBD() {
		List<Medicalstuff> results = entityManager.createNativeQuery("Select * from petshop.medicalstuff", Medicalstuff.class).getResultList();
//		for (Medicalstuff med : results) {
//			System.out.println("Medical Stuff:" + med.getName() + " has ID:" + med.getIdMedicalStuff()+"he is:"+ med.getType());
//		
//		}
		return results;
	}
	/**
	 *  The function that will delete an medical stuff 
	 * @param id of themedical stuff
	 */
	public void deleteMedicalStuff(int id) {
		Medicalstuff med = entityManager.find(Medicalstuff.class, id);
		entityManager.remove(med);
	}
	/**
	 * The function that make up date 
	 * @param med with this will be make the up date
	 * @param id of the object that will be up dated
	 */
	public void upDateMedicalStuff(Medicalstuff med, int id) {
		Medicalstuff m = entityManager.find(Medicalstuff.class, id);
		m.setName(med.getName());
		m.setType(med.getType());
	}
	/**
	 *  the function that will save the appointment in database
	 * @param app the appointment that will be save
	 */
	public void saveAppointement(Appointment app) {
		entityManager.persist(app);
	}
	/**
	 * 
	 * @param app
	 * @param date
	 * @param hour
	 * @param id
	 * @param idA
	 * @param idM
	 */
	public void createAppointment(Appointment app, Date date, Time hour,  int id, int idA, int idM) {
		app.setIdappointment(id);
		app.setIdAnimal(idA);
		app.setIdMedicalStuff(idM);
		app.setDate(date);
		app.setHour(hour);
	}
	/**
	 * Print all the appointment from the database
	 */
	public List<Appointment> printAllAppointmentFromBD() {
		List<Appointment> results = entityManager.createNativeQuery("Select * from petshop.appointment", Appointment.class).getResultList();
//		for (Appointment med : results) {
//			Medicalstuff m= entityManager.find(Medicalstuff.class, med.getIdMedicalStuff());
//			Animal a= entityManager.find(Animal.class, med.getIdAnimal());
//			System.out.println("The animal:"+a.getName() +  " has  appointment at:" +m.getName()+" at:"+ med.getDate()+ " "+med.getHour());
//		
//		}
		return results;
	}
	/**
	 * 
	 * @param id the pk of the appointment
	 */
	public void deleteAppointment(int id) {
		Appointment med = entityManager.find(Appointment.class, id);
		entityManager.remove(med);
	}
	/**
	 * 
	 * @param med 
	 * @param id
	 */
	public void upDateAppointment(Appointment med, int id) {
		Appointment m = entityManager.find(Appointment.class, id);
		m.setDate(med.getDate());
		m.setHour(med.getHour());
	}
	/**
	 * The function for sort by the appointment and print the date, name of the
	 * animal, name of the medical stuff
	 */
	public void sort() {
		List<Appointment> results = entityManager
				.createNativeQuery("Select * from petshop.appointment", Appointment.class).getResultList();
		results.sort(Comparator.comparing(Appointment::getDate));
		for (Appointment prog : results) {
			Medicalstuff m= entityManager.find(Medicalstuff.class, prog.getIdMedicalStuff());
			Animal a= entityManager.find(Animal.class, prog.getIdAnimal());
			System.out.println(prog.getDate() + " -" + m.getName() + " -"
					+ "animal Name"+(a.getName()));
		}
	}
	
	public List<Istoric> printAllIstoricFromBD() {
		List<Istoric> results = entityManager.createNativeQuery("Select * from petshop.istoric", Istoric.class).getResultList();
		
		return results;
	}
	public void createIstoric(Istoric istoric, int idA, int id, String type) {
		istoric.setIdIstoric(id);
		istoric.setType(type);
		istoric.setIdAnimal(idA);
	}
	
}