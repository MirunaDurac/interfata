package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the medicalstuff database table.
 * 
 */
@Entity
@NamedQuery(name="Medicalstuff.findAll", query="SELECT m FROM Medicalstuff m")
public class Medicalstuff implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int idMedicalStuff;

	private String name;

	private String type;

	//bi-directional many-to-one association to Appointment

	private List<Appointment> appointments;

	public Medicalstuff() {
	}

	public int getIdMedicalStuff() {
		return this.idMedicalStuff;
	}

	public void setIdMedicalStuff(int idMedicalStuff) {
		this.idMedicalStuff = idMedicalStuff;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<Appointment> getAppointments() {
		return this.appointments;
	}

	public void setAppointments(List<Appointment> appointments) {
		this.appointments = appointments;
	}

//	public Appointment addAppointment(Appointment appointment) {
//		getAppointments().add(appointment);
//		appointment.setMedicalstuff(this);
//
//		return appointment;
//	}
//
//	public Appointment removeAppointment(Appointment appointment) {
//		getAppointments().remove(appointment);
//		appointment.setMedicalstuff(null);
//
//		return appointment;
//	}

}