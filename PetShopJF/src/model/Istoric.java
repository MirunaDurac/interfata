package model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the istoric database table.
 * 
 */
@Entity
@NamedQuery(name="Istoric.findAll", query="SELECT i FROM Istoric i")
public class Istoric implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idIstoric;

	private int idAnimal;

	private String type;

	public Istoric() {
	}

	public int getIdIstoric() {
		return this.idIstoric;
	}

	public void setIdIstoric(int idIstoric) {
		this.idIstoric = idIstoric;
	}

	public int getIdAnimal() {
		return this.idAnimal;
	}

	public void setIdAnimal(int idAnimal) {
		this.idAnimal = idAnimal;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

}